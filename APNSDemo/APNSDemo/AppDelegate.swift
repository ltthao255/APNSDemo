//
//  AppDelegate.swift
//  APNSDemo
//
//  Created by Le Trong Thao on 8/9/16.
//  Copyright © 2016 Le Trong Thao. All rights reserved.
//

import UIKit
import PushKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PKPushRegistryDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        registerForPushNotifications(application)
        
        return true
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .None {
            application.registerForRemoteNotifications()
            self.voipRegistration()
        }
    }

    //MARK: - APNS
    
    func registerForPushNotifications(application: UIApplication) {
        
        
        let viewAction = UIMutableUserNotificationAction()
        viewAction.identifier = "VIEW_IDENTIFIER"
        viewAction.title = "View"
        viewAction.activationMode = .Foreground
        viewAction.destructive = true

        
        
        let newsCategory = UIMutableUserNotificationCategory()
        newsCategory.identifier = "NEWS_CATEGORY"
        newsCategory.setActions([viewAction], forContext: .Default)
        
        
        let notificationSettings = UIUserNotificationSettings(
            forTypes: [.Badge, .Sound, .Alert], categories: [newsCategory])
        
        application.registerUserNotificationSettings(notificationSettings)
    }
    
    
    
    //MARK: - register remote notification delegate
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
        var tokenString = ""
        
        for i in 0..<deviceToken.length {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        print("APNS Device Token:", tokenString)
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Failed to register:", error)
    }
    
    
    //MARK: - notification handle
    
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        let aps = userInfo["aps"] as! [String: AnyObject]
        print(aps)
    }
    
    
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [NSObject : AnyObject], completionHandler: () -> Void) {
        
        let aps = userInfo["aps"] as! [String: AnyObject]
        if identifier == "VIEW_IDENTIFIER" {
            let view = UILabel.init(frame: CGRectMake(0, 50, 320, 100))
            view.textAlignment = .Center
            view.text = aps["alert"] as! String
            view.backgroundColor = UIColor.greenColor()
            window?.rootViewController?.view.addSubview(view)
        }
        
        completionHandler()
    }
    
    //MARK: - VOIP
    
    // Register for VoIP notifications
    func voipRegistration() {
        let mainQueue = dispatch_get_main_queue()
        // Create a push registry object
        let voipRegistry: PKPushRegistry = PKPushRegistry(queue: mainQueue)
        // Set the registry's delegate to self
        voipRegistry.delegate = self
        // Set the push type to VoIP
        voipRegistry.desiredPushTypes = [PKPushTypeVoIP]
    }
    
    
    // Handle updated push credentials
    func pushRegistry(registry: PKPushRegistry!, didUpdatePushCredentials credentials: PKPushCredentials!, forType type: String!) {
        // Register VoIP push token (a property of PKPushCredentials) with server
        
        let tokenChars = UnsafePointer<CChar>(credentials.token.bytes)
        var tokenString = ""
        
        for i in 0..<credentials.token.length {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        print("VOIP Device Token:",tokenString)
    }
    
    
    func pushRegistry(registry: PKPushRegistry!, didReceiveIncomingPushWithPayload payload: PKPushPayload!, forType type: String!) {
        
        let payloadDict = payload.dictionaryPayload as NSDictionary
        let apsDict = payloadDict.objectForKey("aps") as! NSDictionary
        let message : String = apsDict.valueForKey("alert") as! String
        
        let localNotification = UILocalNotification();
        localNotification.alertBody = message + " [VOIP]"
        localNotification.applicationIconBadgeNumber = 1;
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.userInfo = apsDict as [NSObject : AnyObject]
        
        UIApplication.sharedApplication().presentLocalNotificationNow(localNotification);
        
        
//        //present a local notifcation to visually see when we are recieving a VoIP Notification
//        if UIApplication.sharedApplication().applicationState == UIApplicationState.Background {
//            
//            let localNotification = UILocalNotification();
//            localNotification.alertBody = message! + "[VOIP]"
//            localNotification.applicationIconBadgeNumber = 1;
//            localNotification.soundName = UILocalNotificationDefaultSoundName;
//            
//            UIApplication.sharedApplication().presentLocalNotificationNow(localNotification);
//        }
//            
//        else {
//            
//            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                
//                let alert = UIAlertView(title: "VoIP Notification", message: message, delegate: nil, cancelButtonTitle: "Ok");
//                alert.show()
//            })
//        }
        
    }
    
    func pushRegistry(registry: PKPushRegistry!, didInvalidatePushTokenForType type: String!) {
        
        NSLog("token invalidated")
    }
    
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        
        let apsDict = notification.userInfo
        
        print(apsDict)
        
    }
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

